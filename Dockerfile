# pull official base image
FROM postgres:12.0-alpine

# set work directory
WORKDIR /usr/src/app

# copy project
COPY . .